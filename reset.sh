#!/bin/sh

shopt -s extglob
rm -rf plots
mkdir plots
rm -v !("reset.sh"|*.mbd|"plotter.py"|"plot-many.sh"|"plots"|"freecad-files") 1> /dev/null
mbdyn -f master.mbd
