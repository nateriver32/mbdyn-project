#!/usr/bin/env python

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import re
filename = "MBDCase"

def main():
# Increase plot resolution
    plt.figure(dpi=100)

# Activate text rendering by LaTeX
    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "roman",
        "font.monospace": "Computer Modern",
        "font.size": "12"
    })

# Generate the header
    header = "node x y z o_x o_y o_z v_x v_y v_z omega_x omega_y omega_z nan1 nan2 nan3 nan4 nan5 nan6" + "\n"
    headerReactions = "node f_x f_y f_z t_x t_y t_z F_x F_y F_z T_x T_y T_z ignore1 ignore2 ignore3 ignore4 ignore5 ignore6 ignore7 ignore8 ignore9 ignore10 ignore11 ignore12" + "\n"

# Read the file contents
    with open(f"{filename}.mov", "r") as file:
        lines = file.readlines()

# Eliminate whitespace
    for _ in range(0, len(lines)):
        lines[_] = lines[_].lstrip()

# Write the modified content back to the file
    if lines[0] != header:
        with open(f"{filename}.mov", "w") as file:
            # Modify the first line
            lines.insert(0, header)
            file.writelines(lines)

# Read the file contents
    with open(f"{filename}.jnt", "r") as file:
        lines = file.readlines()

# Eliminate whitespace
    for _ in range(0, len(lines)):
        lines[_] = lines[_].lstrip()

# Write the modified content back to the file
    if lines[0] != headerReactions:
        with open(f"{filename}.jnt", "w") as file:
            # Modify the first line
            lines.insert(0, headerReactions)
            file.writelines(lines)

# Grab the end time variable from .mbd file
    with open(f"{filename}.mbd", "r") as file:
        for line in file:
            if re.search("final time:", line):
                endTime = (line.split(":")[1][:-3])
                endTime = int(endTime)

# Define an independent variable to plot by
        timeStep = 1e-3
        t = np.arange(0, endTime + timeStep, timeStep)

# Format plot
        plt.xlabel("Vrijeme/s")
        plt.ylabel("")
        plt.grid()
        plt.xlim([0, endTime])
        # plt.ylim([ymin, ymax])
        plt.tight_layout()
        # plt.show()

# Plot all user specified nodes
# Generate simulation table
        nodeNumber = sys.argv[1]
        kinematics = pd.read_csv(f"{filename}.mov", delimiter = " ")
        kinematics = kinematics[kinematics["node"].astype(str).str.startswith(nodeNumber)]
        reactions = pd.read_csv(f"{filename}.jnt", delimiter = " ")
        reactions = reactions[reactions["node"].astype(str).str.startswith(nodeNumber)]

# Filter all the headings
        node = kinematics["node"]
        x = kinematics["x"]
        y = kinematics["y"]
        z = kinematics["z"]
        o_x = kinematics["o_x"]
        o_y = kinematics["o_y"]
        o_z = kinematics["o_z"]
        v_x = kinematics["v_x"]
        v_y = kinematics["v_y"]
        v_z = kinematics["v_z"]
        omega_x = kinematics["omega_x"]
        omega_y = kinematics["omega_y"]
        omega_z = kinematics["omega_z"]
        # f_x = reactions["f_x"]
        # f_y = reactions["f_y"]
        f_z = reactions["f_z"]
        t_x = reactions["t_z"]
        t_y = reactions["t_z"]
        t_z = reactions["t_z"]
        # F_x = reactions["F_x"]
        # F_y = reactions["F_y"]
        F_z = reactions["F_z"]
        T_x = reactions["T_z"]
        T_y = reactions["T_z"]
        T_z = reactions["T_z"]

# Plot everything
        # fplot(t, x, "$x$", "#ff0000")
        # fplot(t, y, "$y$", "#00ff00")
        # fplot(t, z, "$z$", "#0000ff")
        # fplot(t, v_x, "$v_x$", "#ff00ff")
        # # fplot(t, v_y, "$v_y$", "#00ffff")
        # fplot(t, v_z, "$v_z$", "#000000")
        # # fplot(t, omega_x, "$\\omega_x$", "#fa5000")
        # fplot(t, omega_y, "$\\omega_y$", "#be84ff")
        # fplot(t, omega_z, "$\\omega_z$", "#707070")
        # fplot(t, f_x, "$f_x$", "#707070")
        # fplot(t, f_y, "$f_y$", "#707070")
        # fplot(t, f_z, "$f_z$", "#ff0000")
        # fplot(t, t_x, "$t_x$", "#707070")
        # fplot(t, t_y, "$t_y$", "#707070")
        # fplot(t, t_z, "$t_z$", "#707070")
        # fplot(t, F_x, "$F_x$", "#707070")
        # fplot(t, F_y, "$F_y$", "#707070")
        fplot(t, F_z, "$F_z$", "#00ff00")
        # fplot(t, T_x, "$T_x$", "#707070")
        # fplot(t, T_y, "$T_y$", "#707070")
        # fplot(t, T_z, "$T_z$", "#707070")

        legend=plt.legend(fontsize="12",
                          frameon=True,
                          fancybox=False,
                          facecolor="white",
                          framealpha=1,
                          loc="upper left")
        legend.get_frame().set_edgecolor("black")

        plt.savefig(f"./plots/node-{sys.argv[1]}.pdf")
        print(f"done plotting node {sys.argv[1]}")

# Other functions
def fplot(x, y, label, color):
    plt.plot(x, y,
             label=label,
             color=color,
             # marker="o",
             markersize="1.5",
             # linestyle="dotted"
             )

main()
