# dizalica.mbd

begin: data;
   problem: initial value;
end: data;

begin: initial value;
   initial time:   0.;
   final time:     5.;
   time step:      1.e-3;
   max iterations: 10;
   tolerance:      1.e-6;
   # output: residual;
   # output: iterations;
end: initial value;

begin: control data;
   structural nodes: 9;
   rigid bodies:     8;
   joints:           14;
   structural nodes: 8;
   rigid bodies:     7;
   joints:           13;
   gravity;
end: control data;

# Design Variables
set: real M = 1.; # Masa svakog stapa
set: real L = 1.; # Duljina svakog stapa
set: real theta = pi/4.; # Pocetni kut odstupanja

# Node Labels
# CS - cvor stapa
set: integer CS1 = 1;
set: integer CS2 = 2;
set: integer CS3 = 3;
set: integer CS4 = 4;
set: integer CS5 = 5;
set: integer CS6 = 6;
set: integer Pod = 7;
set: integer CK = 8;
set: integer CGK = 9;

# Body Labels
# S - stap
# K - klizac
set: integer S1 = 1;
set: integer S2 = 2;
set: integer S3 = 3;
set: integer S4 = 4;
set: integer S5 = 5;
set: integer S6 = 6;
set: integer K = 7;

# Joint Labels
# NO - nepomicni oslonac
set: integer NO1 = 1;
# C - cvor izmedu stapova
set: integer C12 = 12;
set: integer C23 = 23;
set: integer C14 = 14;
set: integer C34 = 34;
set: integer C36 = 36;
set: integer C45 = 45;
set: integer C46 = 46;
# U - ukljestenje
set: integer U = 2;
set: integer UK = 3;
set: integer K2 = 4;
set: integer K6 = 5;
set: integer KP = 6;
set: integer KGP = 7;
set: integer GK = 8;

# Referentni koordinatni sustavi
set: integer RFK6 = 1;
reference: RFK6,
   0, 0., 2.*L*sin(theta), # absolute position
   eye, # absolute orientation
   null,                        # absolute velocity
   null;                        # absolute angular velocity

begin: nodes;

   structural: CS1, dynamic,
     L/2.*cos(theta), 0., L/2.*sin(theta), # absolute position
     euler, 0., -theta, 0.,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   structural: CS2, dynamic,
     L/2.*cos(theta), 0., L/2.*sin(theta), # absolute position
     euler, 0., theta, 0.,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   structural: CS3, dynamic,
     L/2.*cos(theta), 0., 3.*L/2.*sin(theta), # absolute position
     euler, 0., -theta, 0.,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   structural: CS4, dynamic,
     L/2.*cos(theta), 0., 3.*L/2.*sin(theta), # absolute position
     euler, 0., theta, 0.,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   # structural: CS5, dynamic,
   #   3*L/4.*cos(theta), 0., 5.*L/4.*sin(theta), # absolute position
   #   euler, 0., pi/6, 0.,             # absolute orientation
   #   null,            # absolute velocity
   #   null;            # absolute angular velocity

   structural: CS6, dynamic,
     reference, RFK6, L/2., 0., 0., # absolute position
     eye,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   structural: CK, dynamic,
     L*cos(theta), 0., 0., # absolute position
     eye,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   structural: CGK, dynamic,
     reference, RFK6, L*cos(theta), 0., 0., # absolute position
     eye,             # absolute orientation
     null,            # absolute velocity
     null;            # absolute angular velocity

   structural: Pod, static,
     null,       # absolute position
     eye,        # absolute orientation
     null,       # absolute velocity
     null;       # absolute angular velocity

end: nodes;

begin: elements;
   body: S1, CS1,
     M,                                # mass
     null,                             # relative center of mass
     diag, 0., M*L^2./12., M*L^2./12.; # inertia matrix

   body: S2, CS2,
     M,                                # mass
     null,                             # relative center of mass
     diag, 0., M*L^2./12., M*L^2./12.; # inertia matrix

   body: S3, CS3,
     M,                                # mass
     null,                             # relative center of mass
     diag, 0., M*L^2./12., M*L^2./12.; # inertia matrix

   body: S4, CS4,
     M,                                # mass
     null,                             # relative center of mass
     diag, 0., M*L^2./12., M*L^2./12.; # inertia matrix

   # body: S5, CS5,
   #   M,                                # mass
   #   null,                             # relative center of mass
   #   diag, 0., M*L^2./12., M*L^2./12.; # inertia matrix

   body: S6, CS6,
     M,                                # mass
     null,                             # relative center of mass
     diag, 0., M*L^2./12., M*L^2./12.; # inertia matrix

   body: K, CK,
     M,                                # mass
     null,                             # relative center of mass
     eye;                              # inertia matrix

   body: GK, CGK,
     M,                                # mass
     null,                             # relative center of mass
     eye;                              # inertia matrix

   joint: NO1, 
     revolute pin, 
        CS1, 
          -L/2., 0., 0.,                                 # relative offset
          hinge, 1, 1., 0., 0., 3, 0., 1., 0.,  # relative axis orientation
          null,                                 # absolute pin position
          hinge, 1, 1., 0., 0., 3, 0., 1., 0.;  # absolute pin orientation

   joint: C12, 
     revolute hinge, 
        CS1,
           0., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
        CS2,
           0., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation

   joint: C23, 
     revolute hinge, 
        CS2,
           -L/2., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
        CS3,
           -L/2., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation

   joint: C14, 
     revolute hinge, 
        CS1,
           L/2., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
        CS4,
           L/2., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation

   # joint: C34, 
   #   revolute hinge, 
   #      CS4,
   #         0., 0., 0.,                     # relative offset
   #         hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
   #      CS3,
   #         0., 0., 0.,                     # relative offset
   #         hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation

   joint: C34,
     in line,
        CS4,
           null,                          # relative line position
           1, 0., 0., -1., 3, 0., 1., 0., # relative orientation
        CS3;

   # joint: C45, 
   #   revolute hinge, 
   #      CS4,
   #         L/4., 0., 0.,                     # relative offset
   #         hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
   #      CS5,
   #         0., 0., 0.,                     # relative offset
   #         hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation

   joint: C46, 
     revolute hinge, 
        CS4,
           -L/2., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0., # relative axis orientation
        CS6,
           reference, RFK6, 0., 0., 0.,                     # relative offset
           hinge, 1, 1., 0., 0., 3, 0., 1., 0.; # relative axis orientation

   joint: U,
     clamp,
        Pod,
           null, # absolute position
           eye;  # absolute orientation

   joint: UK,
     in line,
        Pod,
           null,                          # relative line position
           1, 0., 0., -1., 3, 1., 0., 0., # relative orientation
        CK;

   joint: GK,
     in line,
        Pod,
           0., 0., 2.*L*sin(theta),                          # relative line position
           1, 0., 0., -1., 3, 1., 0., 0., # relative orientation
        CGK;

   joint: KP,
     prismatic,
        Pod,
        CK;

   joint: KGP,
     prismatic,
        Pod,
        CGK;

   joint: K2,
     in line,
        CS2,
           L/2., 0., 0.,                          # relative line position
           1, 0., 0., -1., 3, 0., 1., 0., # relative orientation TU ROTIRA OKO y
        CK;

   joint: K6,
     in line,
        CS6,
           reference, RFK6, L*cos(theta), 0., 0.,                          # relative line position
           1, 0., 0., -1., 3, 0., 1., 0., # relative orientation TU ROTIRA OKO y
        CGK;

   gravity: 0., 0., -1., const, 9.81;

end: elements;
